<?php
// forum.php: Product search and display

$search_criteria = $_REQUEST["search_criteria"];
if (!$search_criteria) {
	$search_criteria="all";
}

?>
			<h1>Our Products</h1>
			<form name=loginform method=POST action="<?php echo $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']?>">
				<table width="500">
					<tr>
						<td width="120">Search criteria:</td>
						<td width="215"><input type=text name=search_criteria size=30 autocomplete=no value="<?php 
              echo $search_criteria; ?>"></td>
						<td><input type=submit name=submit_search value="Search"></td>
					 </tr><tr>
						<td colspan="2"><p>(Enter the product description search criteria in the text box above e.g."machine"; "all" displays all products.)</p></td>
					 </tr>
				</table>
			</form>
			<h2>Results for search '<?php echo $search_criteria; ?>':</h2>
			<table width="100%">
				<thead><tr class="head">
					<td class="">Prod. Code</td>
					<td class="">Price (�)</td>
					<td class="">Description</td>
				</tr></thead>
				
<?php
	if (strtolower($search_criteria) == "all") $search_criteria="";
	$query  = "SELECT pcode,price,description FROM products WHERE description like '%" . $search_criteria . "%' ORDER BY PRICE ";
	
	$result = execute_query($query);
	
	$i=0;
	while($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		if ($i++%2 == 0) $trow = "even"; else $trow = "odd"; 
		echo "				<tr class='{$trow}'>";
		echo "					<td>{$row['pcode']}</td><td>{$row['price']}</td><td>{$row['description']}</td>";
		echo "				</tr>";
	}
	if ($i==0) {
		echo "<tr class='even'><td colspan='3'><h3>No Results.</h3></td></tr>";
	}
?>
			</table>
