<?php
require_once("user.php");

include 'includes/dbconfig.inc';
include 'includes/db.inc';

// Allow users to use the back button without reposting data
header ("Cache-Control: private");

// Grab inputs
$username = $_REQUEST["user_name"];
$password = $_REQUEST["password"];
$action = $_REQUEST["action"];
if (isset($_POST['submit_login'])) {
	$action = "login";
}if (isset($_POST['submit_registration'])) {
	$action = "registration";
}

$user = new User($db);

switch ($action) {
	case "logout":
		$user->_logout();
		break;
	case "login":
		if (verifyLoginFields($username, $password)) {
			if ($user->_login($username, $password)) {
				echo '<meta http-equiv="refresh" content="0;url=index.php">';
			} else {
				$login_register_result = "Invalid username or password.";
			}
		}
		break;
	case "registration":
		if (verifyLoginFields($username, $password)) {
			if ($user->username == "admin") 
			{
				if ($user->_register($username, $password)) {
					$login_register_result = "User registered.";
				} else {
					$login_register_result = "Error registering user.";
				}
			} else {
				$login_register_result = "Error registering user: Must be user admin";				
			}
		}
		break;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Lemonade - A Bitterly Insecure Web Application</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
<link href="style.css" rel="stylesheet" type="text/css"/>

<!-- Begin IE Fixes (Don't touch unless you know what you're doing!)-->
	<!--[if IE]>
	<link type="text/css" href="ie.css" rel="stylesheet" />
	<![endif]-->
<!-- End of IE fixes -->

</head>
<body>

<div id="container">
	<div id="navigation">
		<ul>
		<li><a href="index.php">Home</a></li>
		<li><a href="index.php?page=products">Our Products</a></li>
		<li><a href="index.php?page=forum">Forum</a></li>
		<div id="login">
			<?php
			if ($user->id != 0 && $user->username != null) {
				if ($user->username == "admin") echo '<a href="index.php?page=register">register user</a>&nbsp';
				echo '<a href="index.php?action=logout">logout ' . $user->username . '</a>';
			} else {
				echo '<a href="index.php?page=login">login</a>';
			}
			echo "		</div>";
			if ($action == "logout") {
				echo '<meta http-equiv="refresh" content="0;url=index.php">';
			}
		?>

		</ul>
	</div>
  		<div id="header">
		  <h1>lemonade.</h1>
<?php
  // Pick a random title for the page... 
  srand();
  $sentences = array( "is a uniquely sweet and sour liquid",
					"the sweet and sour taste of hard work",
					"sweet learning, bitter work",
					"refreshing the students of ESEGI",
					"refreshing the minds of students",
					"cooling the student's warm hearts",
					"the fresh sensation of insight");
  $sentence = $sentences[array_rand($sentences)];
  echo "		  <h2>$sentence</h2>";		  
?>
		</div>
		<div id="wrapper">
		<div id="content">
		<!-- Begin Content -->
